<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class Product extends  REST_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct(){
	 parent::__construct();
	 $this->load->model('ProductModel');
	 }
	
	public function index_get($id = null)
	{
		$data = $this->ProductModel->getProduct($id);
		$this->response($data);
		//echo "test get";
	}
	
		
	public function index_post()
	{
		//$this->load->view('welcome_message');
		//echo "test post";

		$dataPost=json_decode($this->input->raw_input_stream,true);
		$data = $this->ProductModel->insertProduct($dataPost);
		$this->response($data);
	}


	 /**
	public function test()
	{
		echo "test123";
	}	
	**/
}
