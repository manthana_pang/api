<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	private $table_name = "product";
	public function getProduct($id)
	{
		//$sql="SELECT * FROM product";
		$sql="SELECT * FROM " . $this->table_name;
		if ($id) {
			$sql.=" WHERE id = '" .$id ."'";
			}

		
		$query=$this->db->query($sql);
		
		return $query->result_array();
		//$this->load->view('welcome_message');
	}
	
	
	public function insertProduct ($data)
	{
		$product['name']=isset($data['name'])?$data['name']:"";
		$product['price']=isset($data['price'])?$data['price']:0;
		$product['stock']=isset($data['stock'])?$data['stock']:0;

		//$this->db->insert($this->$table_name,$product);

		if ($this->db->insert($this->table_name,$product)){
			$res['status']=true;
			$res['message']=$this->db->insert_id();
			return $res;
		} else {
			$res['status']=false;
			$res['message']="err";
			return $res;
		}
	}

	public function test()
	{
		echo "test123";
	}



}
